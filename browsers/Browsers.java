package browsers;

  abstract class Browsers {
   // private String browserName = "";
   // private String defaultPage ="";

//    //public String getBrowserName(){
//        return browserName;
     abstract String getBrowserName();
//    }
//    public void setBrowserName (String browName){
//        browserName = browName.trim(); //trims any space at beginning and end of string
//    }
      abstract void setBrowserName(String name);
    //Getters and setters allow user to interact with variables
    // indirectly through get and set methods
//    public String getDefaultPage() {
//        return defaultPage;
//    }
      //abstract String getdefaultPage();
//    public void setDefaultPage(String page) {
//        defaultPage = page;
      abstract void setDefaultPage();
      public void openDefaultHomePage(){
          System.out.println("opened");
      }
    }


class ChromeBrowser extends Browsers {
    private String browserName = "";
    private String defaultPage = "google.com";

    void setDefaultPage() {
        System.out.println("Default page= " + defaultPage);
    }
    void setBrowserName(String name) {
        browserName = name;

    }
    String getBrowserName(){
        return browserName;
    }
}