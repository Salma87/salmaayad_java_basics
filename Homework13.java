import java.util.HashMap;

public class Homework13 {
    public static void main(String[]args){
        HashMap<Integer, String> neighborhood = new HashMap<>();
        neighborhood.put(1000, "Liam");
        neighborhood.put(1001, "Noah");
        neighborhood.put(1002, "Olivia");
        neighborhood.put(1003, "Emma");
        neighborhood.put(1004, "Benjamin");
        neighborhood.put(1005, "Evelyn");
        neighborhood.put(1006, "Lucas");
       // System.out.println(neighborhood);
        //Find the name of the person who lives at 1004
        System.out.println(neighborhood.get(1004) + " lives on 1004 street");
        //print out all the odd number streets and its corresponding names
        for (Integer i : neighborhood.keySet()){
            if (i % 2 != 0){
                System.out.println("Street number: " + i + " corresponds to: " + neighborhood.get(i));
            }
        }
    }
}
