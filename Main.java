public class Main {
    public static void main(String[] args) {
        try {
            int[] arr = {1, 2, 3};
            System.out.println(arr[3]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("This is why QA Engineers always have to do boundary testing!The array only has 3 values and you've requested a 4th");
        }

    }
}


