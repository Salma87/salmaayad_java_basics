import java.util.*;

public class Homework12 {
    public static void main(String[]args){
        Set<String> cohortOne = new HashSet<String>();
        cohortOne.add("USA");
        cohortOne.add("USA");
        cohortOne.add("Ukraine");
        cohortOne.add("Mexico");
        Set<String> cohortTwo = new HashSet<String>();
        cohortTwo.add("USA");
        cohortTwo.add("Canada");
        cohortTwo.add("USA");
        cohortTwo.add("Mexico");
        //Ordered cohorts
        Set<String> cohortOneOrdered = new TreeSet<String>(cohortOne);
        Set<String> cohortTwoOrdered = new TreeSet<String>(cohortTwo);
        //Print out the list of all unique countries of every cohort separately
        System.out.println("Cohort One: " + cohortOne);
        System.out.println("Cohort Two: " + cohortTwo);
        //Print out all unique countries from all cohorts in one line
        Set<String> combinedCohorts = new HashSet<String>();
        combinedCohorts = cohortOne;
        combinedCohorts.addAll(cohortTwo);
        System.out.println("Combined Cohorts: " + combinedCohorts);
        //Print out all unique countries from all cohorts in alphabetical order
        System.out.println("Cohort One ordered: " + cohortOneOrdered);
        System.out.println("Cohort Two ordered: " + cohortTwoOrdered);
        //Print out the common countries from both cohorts
        Set<String> commonCountries = new TreeSet<String>();
        commonCountries = cohortOneOrdered;
        commonCountries.retainAll(cohortTwoOrdered);
        System.out.println("Common countries ordered: " + commonCountries);
    }
}
