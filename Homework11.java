import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
public class Homework11 {
    public static void main(String[] args){
        List<Integer> arrayOne = new ArrayList<Integer>(Arrays.asList(-2,-3,6,3));
        List<Integer> arrayTwo = new ArrayList<Integer> (Arrays.asList(-2,-4,5,-3));
        List<Integer> arrayThree;
        arrayOne.addAll(arrayTwo);
        arrayThree = arrayOne;
        //Finding minimum value using a loop
        Integer min = arrayThree.get(0);
        for (int i=0; i<arrayThree.size();i++){
            if(arrayThree.get(i)<min){
                min=arrayThree.get(i);
            }
        }
        System.out.println("Min value of arrayThree is: " + min);
        //Another way using min method
        System.out.println("Min Value of arrayThree is: "+Collections.min(arrayThree));
        //Another way using sort method and getting 1st value from array
        Collections.sort(arrayThree);
        System.out.println("Min Value of arrayThree is: " + arrayThree.get(0));
    }
}



