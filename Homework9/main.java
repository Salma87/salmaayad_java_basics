package Homework9;
public class main {
    public static void main (String[]args){
        LinkElement clickOnLink = new LinkElement();
        TextElement clickOnText = new TextElement();
        System.out.println(clickOnLink.locator);
        System.out.println(clickOnText.locator);
        //We see the same locator because they both inherited it from parent
        // class BaseElement (Inheritance principle)
        clickOnText.click();
        clickOnLink.click();
        //Polymorphism principle
    }
}

